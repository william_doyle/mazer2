#include "Maze.hpp"
#include <ctime>

int main() {
	srand(time(NULL));
	Maze* maze = new Maze(500);
	
	std::cout << "Maze created\n";
	
	if (maze->ImageGenerate("test.ppm")){
		std::cout << "Image created and saved\n";
	}
	else {
		std::cout << "Could not save an image of the maze!\n";
	}
	delete maze;
	

	/*
	// generate a few smaller mazes to chek they are solvable
	int mazeId = 0;
	for (int i = 0; i < 15; i++){
		Maze m = Maze((rand()%50)+5);
		m.ImageGenerate("tests/maze_" + std::to_string(i) + ".ppm");
	}
	*/
}
