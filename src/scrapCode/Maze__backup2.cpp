#include "Maze.hpp"
#include <fstream>
#include <stack>


/**
 *	Maze.cpp
 *	William Doyle
 *	Late October 2020 ... Continuing
 *	Implementations of functions belonging to the Maze class
 *
 *
 *
 * */


/* 	Maze
 *  	()[]
 *	William Doyle
 *	Construct the maze object by:
 *		1. setting attributes to according parameters
 *		2. Generating the nodes behind the maze and setting up the parameter walls (including a starting hole in the top and a goal hole in the bottom)
 *		3. Settings up the internal walls of the maze
 *
 *
 *
 * */
Maze::Maze(unsigned char _length, unsigned char _width, unsigned char _start_pos_hoz_index, unsigned char _goal_point_hoz_index ){
	// Set basic attributes
	this->length = _length;
	this->width = _width;
	this->startPosX = _start_pos_hoz_index;
	this->goalPosX = _goal_point_hoz_index;
	
	// top left corner of the grid. 
	this->topLeftCorner = new GridNode(true);

	GridNode* leftwardConnection;
	leftwardConnection = this->topLeftCorner;

	GridNode* pointerToStartOfLastRow;
	pointerToStartOfLastRow = this->topLeftCorner; 
	
	// build first row
	for( unsigned i = 0; i < this->width; i++){
		GridNode* newGridNode = new GridNode((i == this->startPosX)? false:true); // make top wall with start point "gap"
		newGridNode->Connect(2/*left*/, leftwardConnection);
		leftwardConnection = newGridNode;	
	}

	GridNode* previousRowHead = this->topLeftCorner; 
	for (unsigned k = 0; k < this->length; k++){
		
		GridNode* rowHead = new GridNode(true);
		rowHead->Connect(0/*up*/, previousRowHead);
		leftwardConnection = rowHead;

		// build next row connected to last row
		for( unsigned i = 0; i < this->width; i++){
			GridNode* newGridNode;
		       	if (k == this->length-2){ 
				newGridNode = new GridNode((i == this->goalPosX)? false:true); // this is the last row in the maze. The bottom. Make all nodes wall unless they are they goal node
			}
			else  {
				newGridNode = new GridNode((i == this->width-2)? true:false);  // this is the last coloum in the maze. The last wall to the right. All these nodes are the right most wall.

			}
			newGridNode->Connect(2/*left*/, leftwardConnection);
			leftwardConnection = newGridNode;	
		}

		// connect row to previous row
		GridNode* oldRowIndex = previousRowHead;
		GridNode* rowIndex = rowHead;

		for( unsigned i = 0; i < this->width; i++){
			rowIndex->Connect(0/*up*/, oldRowIndex);
			
			if (i == this->width){
				break;
			}
			else {
				rowIndex = rowIndex->right;
				oldRowIndex = oldRowIndex->right;
			}
		}

		previousRowHead = rowHead;
	}
	
	
//	std::cout << "STUB\t" << __func__ << " Width: " << this->DetermineLengthFromNode(1, this->topLeftCorner) << " Length: " << this->DetermineLengthFromNode(3, this->topLeftCorner) << "\n";

	// Generate walls right here
	this->RecursiveRangeWallDivision(this->topLeftCorner, this->length, this->width); // divide maze with walls using this recursive function

}



/**
 *	ImageGenerate
 *	(string x1)[filename of the image]
 * 	William Doyle
 **/
bool Maze::ImageGenerate(std::string filename){
	try{
		std::ofstream ofs(filename, std::ios_base::out | std::ios_base::binary); 
		ofs << "P6" << std::endl << (int)this->width << ' ' << (int)this->length << std::endl << "255" << std::endl;	

		GridNode* rowHead = this->topLeftCorner; // this is the first element in the row. start at the top left

		for(int i = 0; i < this->length; i++){
			GridNode* rowIndex = rowHead; // start at the first node in the row
			for (int k = 0; k < this->width; k++){
				
				if (rowIndex->state == true){
					ofs << (unsigned char)(100u) << (unsigned char)(100u) << (unsigned char)(100u);  
				}
				else {
					ofs << (unsigned char)(205u) << (unsigned char)(255u) << (unsigned char)(255u);  
				}
				
				if (rowIndex->right){
					rowIndex = rowIndex->right;
				}
				else {
					break;
				}

			}
			if (rowHead->down){
				rowHead = rowHead->down;
	//			std::cout << "Moving on to next row of pixels in maze image\n";
			}
			else {
				break;
			}
		}


		ofs.close();

	} catch (...) {
		// Report that some issue occured
		std::cout << "[Error] An issue occured while attempting to save an image of this maze.\n";
		return false; // indicate failure to caller
	}
	return true;
}



/**
 *
 *
 * */
Maze::Maze(unsigned char  _size,  unsigned char  _start_pos_hoz_index, unsigned char  _goal_point_hoz_index ){
	Maze(_size, _size, _start_pos_hoz_index, _goal_point_hoz_index );
}


/**
 *	~Maze
 *	()[]
 *
 * */
Maze::~Maze(){
	// Delete the maze
	// Traverse maze and delete each node on the way back up
	
}

int Maze::DetermineLengthFromNode(short direction, GridNode* startNode){
	int nodeCount = 0;
	GridNode* currentNode = startNode;
	switch (direction) {
		case 0: 
		case 2:
			std::cout << __func__ << " not a valid value for direction\n";
			return -1;
		case 1:	// down
			while (currentNode->down){
				if (currentNode->state == false){	// break if we hit a wall
					if (currentNode != startNode){
						break;
					}
				}
				nodeCount++;
				currentNode = currentNode->down;

			}
			break;
		case 3: // right
			while (currentNode->right){
			//	if (currentNode->state == false){	// break if we hit a wall
			//		if (currentNode != startNode){
			//			break;
			//		}
			//	}
				nodeCount++;
				currentNode = currentNode->right;
				if (currentNode->right == nullptr){
					break;
				}
			}
			break;

	}
//	std::cout << __func__ << " node cout = " << nodeCount << " Direction: " << ((direction == 1)? "down":"right") << "\n";
	return nodeCount;

}



/**
 *	William Doyle
 *	November 2nd 2020
 * */
void Maze::RecursiveRangeWallDivision(GridNode* localChamberTopLeft, unsigned chamberLength, unsigned chamberWidth){
	

	std::cout <<  __func__ << " called! \n";
	
	// if we are below the recursive limit return
	if ( (chamberWidth < 1) || (chamberLength < 1) ) { // we are below the recursive limit
		return;
	}
	
	// find a random place to put some walls and a random place to put some holes in those walls
	int randomWallX = chamberWidth / 2;//rand() % chamberLength;
	// make sure walls are even numbers and holes are odd.	
	if (randomWallX %2 == 1){
		// wall is in odd position
		if (randomWallX-1 < 1){
			randomWallX += 1;
		}
		else {
			randomWallX -= 1;
		}
	}


		
	
	int WallX_YHole	= rand() % chamberLength;
	WallX_YHole = (WallX_YHole % 2 == 1)? WallX_YHole: WallX_YHole-1;

	int randomWallY = rand() % chamberLength; 
	randomWallY = (randomWallY % 2 == 0)? randomWallY:randomWallY-1;
	int WallY_XHole	= rand() % chamberWidth;
	WallY_XHole = (WallY_XHole % 2 == 1)? WallY_XHole: WallY_XHole-1;
	

	int flipper = 0;//rand()%2;	
	
	
	
	// flip the switched on the grid nodes to build walls
	GridNode* rowHead = localChamberTopLeft; // start value for current node
	for (int i = 0; i < chamberLength; i++){
		GridNode* rowIndex = rowHead;
		for (int k = 0; k < chamberWidth; k++) {
	
	


			if (flipper) {
				// B U I L D   H O R I Z O N T A L   W A L L 
				if ((i == randomWallX)&&(k != WallX_YHole)){
					rowIndex->state = true;
				}
	
				if ((i == 0) && (k == 0)){
					// T O P  
		//			this->RecursiveRangeWallDivision(rowIndex, randomWallY, chamberWidth);
				}
				if ((i == randomWallY)&&(k == 0)){
					// bottom 
		//			this->RecursiveRangeWallDivision(rowIndex, chamberLength - randomWallY, chamberWidth);

				}

			}
			if (!flipper) {
				// B U I L D   V E R T I C A L   W A L L 
				if ((i != WallY_XHole )&&(k == randomWallY)){
					rowIndex->state = true;
				}
				if ((i == 0 )&&( k == 0)){
					// left  
	//				this->RecursiveRangeWallDivision(rowIndex, chamberLength, chamberWidth - randomWallX -1 );
				}
				if ((i == randomWallY )&&(k == 0)){
					// left side of wall
			//		this->RecursiveRangeWallDivision(rowIndex, chamberLength, chamberWidth - randomWallX +1);
				}

			}



			// change node states to 'true' in order to 'draw' the dark parts of the maze... (aka 'the walls')
			if ((i == randomWallX)&&(k != WallX_YHole)){
				rowIndex->state = true;
			}
			else if ((k == randomWallY)&&(i != WallY_XHole )){
				rowIndex->state = true;
			}
			
	

			// iterate row index
			if (rowIndex->right){
				rowIndex = rowIndex->right;
			}
			else {
				break;
			}
		}
		if (rowHead->down){
			rowHead = rowHead->down;
		}
		else {
			break;
		}

	}
	static int rcount = 0;
	this->ImageGenerate("shot_" + std::to_string(rcount++) + ".ppm");

	return;	

}


