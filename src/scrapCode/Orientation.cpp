
#include "Orientation.hpp"

std::string Orientation::GetState() {
	return this->state;
}

bool Orientation::SetState (std::string sCandidateValue) {
	if ((sCandidateValue[0] == 'v')||(sCandidateValue[0] == 'V'))
		this->state = "Vertical";
	else if ((sCandidateValue[0] == 'h')||(sCandidateValue[0] == 'H'))
		this->state = "Horizontal";

	else 	// reject other values
		return false;
	return true;


}
