#include "Maze.hpp"

/*
 *	November 7th 2020
 *	William Doyle
 *	MazeAddFunc.cpp
 *
 *	These are functions belonging to the Maze Class because I did not 
 *
 * */


/**
 *	November 7th
 *	William Doyle
 *	WorkWall()
 *
 * */
int Maze::WorkWall(GridNode* cursor, unsigned holeLimiter, unsigned chamberLimiter, bool (*GRIDNODE_ITERATOR_A)(GridNode**),  bool (*GRIDNODE_ITERATOR_B)(GridNode**), int* wallPos, GridNode** above_ptr, GridNode** below_ptr){
	
	int holePos; // not externally relevent to logic... the position of the hole in this wall 

	const int tryLimit = 20;
	int tryCount = 0;
	*wallPos = (chamberLimiter/2);
	

	if (*wallPos >= chamberLimiter ){
	//	std::cout << "Logic kick up from " << __func__ << " [NOTICE: WALLPOS TO BIG (wallPos " << *wallPos << ", chamberLimiter " <<chamberLimiter <<  ")] \n";
		return -1;
	}
	tryCount = 0;
	holePos = (holeLimiter%2 == 0)? holeLimiter-3:holeLimiter/2; 

	// advance to first node in new wall
	for (int i = 0; i <= *wallPos; i++){
		// advance iterator A
		if (GRIDNODE_ITERATOR_A(&cursor) == false){
			break;
		}
	}
	// cursor is currently on the wall.. save this value for the caller to manually advance
	*below_ptr = cursor;


	for (int i = 0; i < holeLimiter; i++){
		if (i != holePos){
			cursor->state = true;
		}
		// advance iterator  B
		if (GRIDNODE_ITERATOR_B(&cursor) == false )
		{
		//	std::cout << "Logic kick up from " << __func__ << " [NOTICE: ITERATOR B FAILED] \n";
			return -1;
		}
		//this->ImageSave()	static unsigned long imageNumber = 0;
	//	this->ImageGenerate("manyimg/" + std::to_string(imageNumber++)+ ".ppm");


	}
	return 1;

}
