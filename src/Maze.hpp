/*
 *	Describe the Class Maze....
 *		This is an abstraction of a maze. A maze is a path or colection of paths from an entrance to a goal.
 *
 * 	Author: William Doyle
 *	October 28th 2020
 *
 * */
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "GridNode.hpp"

class Maze  {


	private:
		unsigned char length, width, startPosX, goalPosX;
		GridNode* topLeftCorner;
		[[deprecated]]
		int DetermineLengthFromNode(short direction, GridNode* startNode);
	
		class RecursiveDivisionCallData {
		public: 
			unsigned chamberLength;
			unsigned chamberWidth;
			GridNode* startNode;
			RecursiveDivisionCallData (GridNode* , unsigned, unsigned);
		};
	public:

	
	       GridNode* goal;


	

		/*	William Doyle
		 *	November 12th 2020
		 *	Use this constructor for some nice assumptions
		 *
		 * */
		Maze(unsigned int _size);

		/**
		 *	William Doyle
		 *	October 28th 2020
		 * */
		~Maze();

		/**
		 *	Generate Image
		 *	used to crudly display the current state of this maze as an image file
		 *	returns true upon successful writing of image of this maze
		 * */
		bool ImageGenerate(std::string filename);
	
		/*
		 *	November 12th 2020
		 *	William Doyle
		 * */
		void GenerateWalls(int xMax, int yMax, GridNode* TLCorner);

		/* 
		 *
		 *
		 * William Doyle
		 * November 7th 2020
		 * used to build a wall
		 * contains most of the non recursive logic used by the RecursiveRangeWallDivision() function \
		 *
		 * Please see Maze Additional function body's file --> MazeAddFunc.cpp for function body 
		 * */
		int WorkWall(GridNode* cursor, unsigned holeLimiter, unsigned chamberLimiter, bool (*GRIDNODE_ITERATOR_A)(GridNode**),  bool (*GRIDNODE_ITERATOR_B)(GridNode**), int* wallPos, GridNode** above_ptr, GridNode** below_ptr);

};	
