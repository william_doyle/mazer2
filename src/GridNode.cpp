#include "GridNode.hpp"

/**
 *		
 * 		William Doyle
 * 		October 28th 2020
 *		(bool x1)[value to set for the current state of this gridnode]
 * */
GridNode::GridNode(bool _state ){
	this->state = _state;
	this->istate = 0;

	this->specialFlag = 0;	

	this->up = nullptr;
	this->down = nullptr;
	this->left = nullptr;
	this->right = nullptr;

	this->xpos = 1;
	this->ypos = 1;
}




/**
 *	William Doyle
 *	Connect
 *	October 28th 2020
 *	(short x1, GridNode* x1)[used to indicate the direction of the new connection, the node to connect to]
 *	Connect two nodes together
 * */
void GridNode::Connect(short direction, GridNode* connection){
	switch (direction){
		case 0: //	up
			this->up = connection;
			this->up->down = this;
			this->ypos = this->up->ypos + 1;
			break;
		case 1: //	down
			this->down = connection;
			this->down->up = this;
			this->ypos = this->down->ypos  - 1;
			break;
		case 2: //	left
			this->left = connection;
			this->left->right = this;
			this->xpos = this->left->xpos +1;

			break;
		case 3: //	right
			this->right = connection;
			this->right->left = this;
			this->xpos = this->right->xpos -1;
			break;
	}	
}


/*
 * 		William Doyle
 * 		October 28th 2020
 *		Swap the state of the node to the opposite of its current state
 *		()[]
 * */
void GridNode::SwitchState(){
	this->state = !this->state;
}
