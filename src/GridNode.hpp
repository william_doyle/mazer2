/**
 *		Author: William Doyle
 *		Date: October 28th 2020 
 *		Filename: GridNode.hpp
 *		Description: Abstraction of a node in a grid with up to four connections
 * */

#pragma once

#include <ostream>


class GridNode {
	// A node in the grid
	public:
		bool visited = false;
		// Connection pointers to other GridNode's 
		GridNode* up;
		GridNode* down;
		GridNode* left;
		GridNode* right;
	
		short specialFlag;	

		unsigned h_pos;
		unsigned v_pos;

		bool state; // walls are true. not walls are false
		int istate; // sometimes using this is better for enriched detail it allows. A node can be on or off but also have additional states
		
		GridNode(bool); // 1 arg constructor

		void SwitchState();

		void Connect(short, GridNode*);


		int xpos;
		int ypos;

	
};
inline std::ostream & operator<< (std::ostream & Str, GridNode const & v) {
			Str << "{UDLR:" << ((v.up == nullptr)? 0:1 ) << ((v.down == nullptr)? 0:1 ) << ((v.left == nullptr)? 0:1) 
				<< ((v.right == nullptr)? 0:1) << ", position: (" << v.xpos << ", " << v.ypos <<") }" ;    
			return Str;
		}	
		

