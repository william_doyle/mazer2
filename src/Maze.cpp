#include "Maze.hpp"
#include <fstream>
#include <stack>

/**
 *	Maze.cpp
 *	William Doyle
 *	Late October 2020 ... Continuing
 *	Implementations of functions belonging to the Maze class
 *
 *
 *
 * */

/**
 *
 * */
Maze::Maze(unsigned int _size) {
	// 1 safty check
	if (_size < 4 ){
		std::cout << "Error: Maze size to small: " << __func__ << "\n";
		return;
	}

	// Set basic attributes
	this->length = _size;
	this->width = _size;
	this->startPosX = 2;
	this->goalPosX = this->width - ( (this->width % 2 == 0)? 4:5);	
		
	// top left corner of the grid. 
	this->topLeftCorner = new GridNode(true);
	this->topLeftCorner->xpos = 1;
	this->topLeftCorner->ypos = 1;

	// Set up tools used to navigate the structure as we create it
	GridNode* leftwardConnection;
	leftwardConnection = this->topLeftCorner;
	GridNode* pointerToStartOfLastRow;
	pointerToStartOfLastRow = this->topLeftCorner; 

	// BUILD the background of the maze.. the topological space for it to excist inside of
	// build first row of matrix
	for( unsigned i = 0; i < this->width; i++){
		GridNode* newGridNode = new GridNode((i == this->startPosX)? false:true); // make top wall with start point "gap"
		newGridNode->Connect(2/*left*/, leftwardConnection);
		leftwardConnection = newGridNode;	
	}

	GridNode* previousRowHead = this->topLeftCorner; 
	for (unsigned k = 0; k < this->length; k++){
		GridNode* rowHead = new GridNode(true);
		rowHead->Connect(0/*up*/, previousRowHead);
		leftwardConnection = rowHead;

		// build next row connected to last row
		for( unsigned i = 0; i < this->width; i++){
			GridNode* newGridNode;
		       	if (k == this->length-2){ 
				newGridNode = new GridNode((i == this->goalPosX)? false:true); // this is the last row in the maze. The bottom. Make all nodes wall unless they are they goal node
			}
			else  {
				newGridNode = new GridNode((i == this->width-2)? true:false);  // this is the last coloum in the maze. The last wall to the right. All these nodes are the right most wall.

			}
			newGridNode->Connect(2/*left*/, leftwardConnection);
			leftwardConnection = newGridNode;	
		}

		// connect row to previous row
		GridNode* oldRowIndex = previousRowHead;
		GridNode* rowIndex = rowHead;

		for( unsigned i = 0; i < this->width; i++){
			rowIndex->Connect(0/*up*/, oldRowIndex);
			if (i == this->width){
				break;
			}
			else {
				rowIndex = rowIndex->right;
				oldRowIndex = oldRowIndex->right;
			}
		}
		previousRowHead = rowHead;
	}

	// Generate the walls
	this->GenerateWalls(this->width, this->length, this->topLeftCorner);

}


/*
 *
 *	William Doyle
 *	GenerateWalls()
 * */

void Maze::GenerateWalls(int xMax, int yMax, GridNode* TLCorner){
/*
	std::cout << "\nxMax: " << xMax << "\n";
	std::cout << "yMax: " << yMax << "\n";
	std::cout << "TLCorner: " << *TLCorner << "\n\n";
*/
#define RECURSIVE_LIMIT	1
	if ((xMax <= RECURSIVE_LIMIT)||(yMax <= RECURSIVE_LIMIT)){
		return;
	}

	if ((xMax > this->width) || (yMax > this->length)){
		std::cout << "clear\n";
		return;
	}
	
	// Split the maze with a wall.
	int orientation = rand()%2;	// 0 is horizontal 1 is vertical
	int magnitude, hole;
	GridNode* NewTLCorner;
		
	// find a place to put the wall	and its hole
	magnitude = ((orientation == 0)? yMax:xMax)/2;
//	magnitude = (rand()%(((orientation == 0)? yMax:xMax)-1)+1);
	if (magnitude % 2 == 0) {
		// wall magnitude must not be even
		if (magnitude > ((orientation == 0)? yMax:xMax)/2)
			magnitude -= 1;
		else 
			magnitude += 1;
	}

	if (!( (magnitude != 1) && !( magnitude + ((orientation == 0)? TLCorner->ypos:TLCorner->xpos) > ((orientation == 0)? this->length:this->width)-1) ))		return;
		
	// find a place to put the hole. it must be on an even numbered node
	hole = (rand()%(((orientation == 0)? xMax:yMax)-1)+1);
	if (hole % 2 == 1){
		// hole position must not be odd
		if (hole > (((orientation == 0)? xMax:yMax)/2))	// stear for the middle of the wall 
			hole -= 1;
		else 
			hole += 1;
	}
	
	// P L A C E   T H E   W A L L   O N   T H E   M A Z E 

	// go to magnitude node
	GridNode* cursor = TLCorner; // start at the top left corner
      	for (int i = 1; i < magnitude; i++ ){
		if (((orientation == 0)?  cursor->down:cursor->right) == nullptr){
			std::cout << "Bad\n";
		}
		cursor = (orientation == 0)?  cursor->down:cursor->right;
	}

	// save for when we want to do the flip side
	NewTLCorner = cursor; // save the current node at cursor
	NewTLCorner->specialFlag = 4;
	
	// Start going down.. as you do make the nodes as wall or not wall
	for (int i = 1; i < ((orientation == 0)? xMax:yMax); i++){
		
		// mark this node as wall or not
		if ( orientation == 0 ) {
			// horizontal
			if (i == hole){
				cursor->istate = 3; // 3 indicates it is a hole in a wall
			}
			else {
			       cursor->state = true;
			       cursor->istate = 1; // 1 indicates a wall
			}
		
			if (cursor->right == nullptr){
				std::cout << "About to go out of range.[o is h]!  " << *cursor << "\n";
			}	
			cursor = cursor->right;		// iterate cursor	

		}
		else {
			// vertical
			if (i == hole){
				cursor->istate = 3; // 3 indicates it is a hole in a wall
			}
			else{
			       cursor->state = true;
			       cursor->istate = 1; // 1 indicates a wall
			}	

			if (cursor->down == nullptr){
				std::cout << "About to go out of range.[o is v]!  " << *cursor << "\n";
			}	
			
			cursor = cursor->down;	// iterate cursos
		}
	}	
	
	// Recursive calls
	if (orientation == 0) {
		// horizontal
		this->GenerateWalls(	xMax,	magnitude,	TLCorner  );
		this->GenerateWalls(	xMax, yMax - magnitude + 1,	NewTLCorner);
	}
	else {
		// vertical
		this->GenerateWalls( 	magnitude,	yMax,	TLCorner);
		this->GenerateWalls(  	xMax - magnitude +1 , yMax, NewTLCorner);
	}
		
}

/**
 *	ImageGenerate
 *	(string x1)[filename of the image]
 * 	William Doyle
 **/
bool Maze::ImageGenerate(std::string filename){
	try{
		std::ofstream ofs(filename, std::ios_base::out | std::ios_base::binary); 
		ofs << "P6" << std::endl << (int)this->width << ' ' << (int)this->length << std::endl << "255" << std::endl;	

		GridNode* rowHead = this->topLeftCorner; // this is the first element in the row. start at the top left
		static unsigned char blueAmount = 0;
		static unsigned char greenAmount = 0;
		for(int i = 0; i < this->length; i++){
			GridNode* rowIndex = rowHead; // start at the first node in the row
			for (int k = 0; k < this->width; k++){
				// blue change to allow view of each pixel
				blueAmount = (k+i)%255; // looks cool
				greenAmount = (i*k)%255;
			/*	if (rowIndex->specialFlag == 4) {
					ofs << (unsigned char)(255u) << (unsigned char)(5u) << (unsigned char)(255u);  
				}
				else if (rowIndex->istate == 5) {
					ofs << (unsigned char)(200u) << (unsigned char)(165u) << (unsigned char)(5u);  
				}
				else */if (rowIndex->state == true){
					/* this node is part of the wall */
					ofs << (unsigned char)(20u) << (unsigned char)(20u) << (unsigned char )(20u);  
				}
			/*	else if (rowIndex->istate == 3) {
					ofs << (unsigned char)(225u) << (unsigned char)(225u) << (unsigned char)(25u);  
				}
			*/	else {
					/* this node is part of the 'path' */
				//	ofs << (unsigned char)(255u) << greenAmount << blueAmount;  
					ofs << (unsigned char)(255u) << (unsigned char)(255u) << (unsigned char)(255u);  
				}
				
				/* Advance Right */	
				if (rowIndex->right){
					rowIndex = rowIndex->right;
				}
				else {
					break;
				}
			}
			/* Advance Down */
			if (rowHead->down){
				rowHead = rowHead->down;
			}
			else {
				break;
			}
		}
		ofs.close(); // close the stream to the image file
	} catch (...) {
		// Report that some issue occured
		std::cout << "[Error] An issue occured while attempting to generate and save an image of this maze.\n";
		return false; // indicate failure to caller
	}
	return true;
}

/**
 *	~Maze
 *	()[]
 *
 * */
Maze::~Maze(){
	// Delete the maze
	// Traverse maze and delete each node on the way back up
}

